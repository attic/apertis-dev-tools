#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

import os
import subprocess

from test_util import should_succeed, should_fail
from test_util import BASE_TAG, BASE_ARCHIVE, SYSROOTS, BAD_SYSROOTS

# Utility functions
def check_result(result, sysroot):
    expected_tag = BASE_TAG.format(*sysroot)
    return result['VerifiedVersion'] == expected_tag

# Test "ade sysroot verify --file" usage
for sysroot in SYSROOTS:
    should_succeed('sysroot', 'verify', '--file', BASE_ARCHIVE.format(*sysroot),
                   check=lambda x: check_result(x, sysroot))

# Test error cases
for sysroot in BAD_SYSROOTS:
    should_fail('sysroot', 'verify', '--file', BASE_ARCHIVE.format(*sysroot))
