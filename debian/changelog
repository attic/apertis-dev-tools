apertis-dev-tools (0.2020.1) apertis; urgency=medium

  * gitlab-ci: Link to the Apertis GitLab CI pipeline definition.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Wed, 15 Jan 2020 11:18:26 +0800

apertis-dev-tools (0.2019.1) apertis; urgency=medium

  * Add verbose option to supress error message

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Thu, 10 Oct 2019 16:19:26 +0530

apertis-dev-tools (0.2019.0) apertis; urgency=medium

  [ Frédéric Danis ]
  * Support username and password set in URL
  * Add support of .netrc file
  * Add username:password tests for sysroot latest and update commands

  [ Ritesh Raj Sarraf ]
  * Handle percent encoded characters

  [ Frédéric Danis ]
  * Fix crash for URLs without password

 -- Emanuele Aina <emanuele.aina@collabora.com>  Fri, 23 Aug 2019 13:33:42 +0000

apertis-dev-tools (0.2019~dev0.1) apertis; urgency=medium

  * Handle the v2019* versioning schema by being less strict

 -- Emanuele Aina <emanuele.aina@collabora.com>  Thu, 04 Apr 2019 20:06:09 +0000

apertis-dev-tools (0.1903.1) 19.03; urgency=medium

  * Mount the user's home directory is r/w at the same path as outside the
    devroot (Apertis: T5626).

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Tue, 08 Jan 2019 11:07:05 +0100

apertis-dev-tools (0.1812.4) 18.12; urgency=medium

  * Install everything from /usr/bin.

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Wed, 28 Nov 2018 12:00:56 +0100

apertis-dev-tools (0.1812.3) 18.12; urgency=medium

  * Depend on systemd-container.

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Wed, 28 Nov 2018 11:41:53 +0100

apertis-dev-tools (0.1812.2) 18.12; urgency=medium

  * Add devroot-enter tool.
  * Change the formatting style for the ade manpages.
  * Fix the regex in gbp.conf.

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Wed, 28 Nov 2018 10:20:14 +0100

apertis-dev-tools (0.1812.1) 18.12; urgency=medium

  [ Ritesh Raj Sarraf ]
  * Add tests/test-export test script (Apertis: T4427).

  [ Andrej Shadura ]
  * Force allocating a PTY when running gdbserver (Apertis: T4463).
  * Build-Depend on flatpak and ostree since test-export requires them.

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Thu, 18 Oct 2018 12:08:02 +0200

apertis-dev-tools (0.1712.1) 17.12; urgency=medium

  [ Frédéric Dalleau ]
  * ade: fix typo
  * ade: Detect gdbserver process termination (Apertis: T4462)

 -- Frédéric Dalleau <frederic.dalleau@collabora.com>  Wed, 10 Jan 2018 10:53:34 +0000

apertis-dev-tools (0.1712.0) 17.12; urgency=medium

  * append `./` when checking image_version file

 -- Héctor Orón Martínez <hector.oron@collabora.co.uk>  Mon, 18 Dec 2017 18:01:59 +0100

apertis-dev-tools (0.1706.2) 17.06; urgency=medium

  [ Justin Kim ]
  * ade: Add --native option

  [ Simon McVittie ]
  * ade info: Don't crash if an entry point has no icon

 -- Simon McVittie <smcv@collabora.com>  Wed, 24 May 2017 17:17:46 +0100

apertis-dev-tools (0.1706.1) 17.06; urgency=medium

  * ade: configure: Don't modify caller-supplied cflags, ldflags in-place
  * ade: configure: Don't ignore caller-supplied arguments (Apertis: T3978)
  * ade: Reject unknown arguments if they will not be used
  * ade: Provide conventional handling for the -- pseudo-option
  * ade: Warn on unknown arguments without -- pseudo-argument
  * ade: Correct order of deprecated passthrough arguments

 -- Simon McVittie <smcv@collabora.com>  Tue, 16 May 2017 17:04:47 +0100

apertis-dev-tools (0.1706.0) 17.06; urgency=medium

  [ Frédéric Dalleau ]
  * ade: Append distro, release, and arch to sysroot file name (Apertis: T3849)

 -- Frédéric Dalleau <frederic.dalleau@collabora.com>  Mon, 08 May 2017 08:11:06 +0000

apertis-dev-tools (0.1703.4) 17.03; urgency=medium

  [ Frédéric Dalleau ]
  * Fix parsing of sysroot id (Apertis: T3777)
  * Fix usage of undeclared InvalidSysrootError (Apertis: T3777)
  * Rework run_cmd in test_util.py to simplify API (Apertis: T3777)
  * Add sysroot tag parser test (Apertis: T3777)
  * Dummy project to exercise ade configure and build (Apertis: T3777)
  * Add sysroot path option to ade configure
  * test-configure to exercise ade configure (Apertis: T3777)

 -- Frédéric Dalleau <frederic.dalleau@¢ollabora.com>  Thu, 30 Mar 2017 17:37:41 +0000

apertis-dev-tools (0.1703.3) 17.03; urgency=medium

  [ Sjoerd Simons ]
  * version: Remove build identifier parsing
  * version: Don't force releases to be named in the form of YY.QQ
    (Apertis: T3555)
  * tests_: Add test for a non-numeric relesae name

 -- Frédéric Dalleau <frederic.dalleau@¢ollabora.com>  Fri, 17 Mar 2017 08:43:51 +0000

apertis-dev-tools (0.1703.2) 17.03; urgency=medium

  [ Thushara Malali Somesha ]
  * debian/control: Add ribchester package dependency

  [ Frédéric Dalleau ]
  * ade: Change build directory for configure and build (Apertis: T3557)
  * make distclean before configuring an existing folder (Apertis: T3557)
  * Use sudo to install bundles on the simulator (Apertis: T3556)

 -- Frédéric Dalleau <frederic.dalleau@¢ollabora.com>  Fri, 10 Mar 2017 10:54:25 +0000

apertis-dev-tools (0.1703.1) 17.03; urgency=medium

  * Add man pages to deb package
  * ade: Use decode() function to bytes object

 -- Justin Kim <justin.kim@collabora.com>  Wed, 04 Jan 2017 19:24:26 +0900

apertis-dev-tools (0.1612.6) 16.12; urgency=medium

  [ Sjoerd Simons ]
  * Add dependency on flatpak
  * Allow all auth methods

  [ Louis-Francis Ratté-Boulianne ]
  * Use default entry point to start application if not specified
  * Wait for the GDB server to be listening before doing anything else
  * Add --no-interactive option to the debug command

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Fri, 16 Dec 2016 15:45:07 +0100

apertis-dev-tools (0.1612.5) 16.12; urgency=medium

  [ Louis-Francis Ratté-Boulianne ]
  * Only give compiler hint if not compiling natively
  * Unpack arguments list when calling run command
  * Fix run command documentation
  * Add utility function to unwrap target if device
  * Add support start a remote application under a debugger

  [ Sjoerd Simons ]
  * Set debug-file-directory when using a sysroot
  * Run gdb as a subprocess, not execv'd
  * New release
    * Supports for running remote applications under a debug (Apertis: T2993)

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Thu, 15 Dec 2016 12:03:04 +0100

apertis-dev-tools (0.1612.4) 16.12; urgency=medium

  * Add dependency on python3-gi

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Thu, 15 Dec 2016 11:12:54 +0100

apertis-dev-tools (0.1612.3) 16.12; urgency=medium

  [ Justin Kim ]
  * Add python3-paramiko dependency

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Thu, 15 Dec 2016 10:55:57 +0100

apertis-dev-tools (0.1612.2) 16.12; urgency=medium

  [ Guillaume Desmottes ]
  * SysrootServer: set SO_REUSEADDR flag on the socket

  [ Sjoerd Simons ]
  * test: Don't hardcode the port to listen on

  [ Louis-Francis Ratté-Boulianne ]
  * Add LatestURL key to sysroot latest command
  * Add 'installed' command to show currently installed sysroot version
  * Add support for authentication when retrieving remote data
  * Add class for installed sysroot
  * Create SysrootManager class to handle sysroot operations
  * Add info command to retrieve information about an ADE object
  * Add device class and option to retrieve the image version
  * Add support for x86_64 architecture
  * Add SDK and Simulator classes to handle these targets
  * Create cli tool to cross-compile against a specific chroot

  [ Sjoerd Simons ]
  * Don't try to parse non-existing configuration file
  * Raise a proper exception, not a string
  * Create /opt/sysroot via tmpfiles
  * Add dummy install target
  * Be a lot more specific about what to install

  [ Guillaume Desmottes ]
  * ade: display sysroot versions in case of mismatch

  [ Sjoerd Simons ]
  * Don't force a keyfile
  * Clarify unsupported architecture error
  * Sort out the various machine type names
  * Don't join absolute paths
  * Actually use a target specific compiler
  * Recommend an ARM cross-compiler

  [ Louis-Francis Ratté-Boulianne ]
  * Add support for project in info command
  * Add export command to create application bundles
  * Add support for bundles in info command
  * Add install and uninstall commands
  * Add run command to start application

  [ Sjoerd Simons ]
  * Raise an exception if an ssh command fails
  * Use ribchesterctl not ribchester-bundle

  [ Louis-Francis Ratté-Boulianne ]
  * Small fixes to bundle creation

  [ Sjoerd Simons ]
  * New release
  * Supports creating a bundle and transferring it to target (Apertis: T2989)
  * Supports cross-compiling applications bundles (Apertis: T2988)

 -- Sjoerd Simons <sjoerd.simons@collabora.co.uk>  Thu, 15 Dec 2016 10:42:36 +0100

apertis-dev-tools (0.1612.1) 16.12; urgency=medium

  [ Louis-Francis Ratté-Boulianne ]
  * Add basic project structure (Apertis: T2984)
  * Add tool to install/update sysroot
  * Add documentation for sysroot management command
  * Add --config option for sysroot command
  * Add output format option (--format) and machine parseable format
  * Add unit tests for sysroot management tool
  * ade: Expand ~ in configured paths
  * Use pkg-config.multiarch file to determine architecture (Apertis: T2984)

  [ Guillaume Desmottes ]
  * debian: add dep on python modules

 -- Guillaume Desmottes <guillaume.desmottes@collabora.co.uk>  Fri, 09 Dec 2016 10:39:54 +0100
